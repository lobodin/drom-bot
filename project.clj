(defproject drom-bot "0.0.1"
  :description "Drom bot in Telegram."
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.2.374"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/tools.logging "0.3.1"]
                 [org.clojars.kostafey/clucy "0.5.5.0"]
                 [clj-time "0.11.0"]
                 [clj-http "2.2.0"]
                 [clj-http-fake "1.0.2"]
                 [pandect "0.6.0"]
                 [com.taoensso/carmine "2.12.2"] ]
  :main drom-bot.core
  :profiles {:test {:resource-paths ["test/resources"]}})
