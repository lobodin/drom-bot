# Drom Bot Ontology

Ontology of events in GA.

## Bot Initialization

* **category** — `command`
* **action** — `start`

## Ad Search

* **category** — `ad_search`
* **action** — `search`
* **label** — `<ad_query>`

## Next Ad

* **category** — `ad_search`
* **action** — `next`
* **label** — `<ad_query>`

## Stop Ad Search

* **category** — `ad_search`
* **action** — `stop`

## Ad Recognition Failed

* **category** — `ad_search`
* **action** — `recognition_failed`
* **label** — `<ad_query>`

## Ad Not Found On Drom

* **category** — `ad_search`
* **action** — `not_found`
* **label** — `<ad_query>`

## City Filter Click

* **category** — `ad_search`
* **action** — `city_filter`

## City Filter Geolocation Click

* **category** — `ad_search`
* **action** — `city_filter_geolocation`

## City Filter Name Input

* **category** — `ad_search`
* **action** — `city_filter_name_input`
* **label** — `<city_name_query>`

## City Filter Geolocation Failed

* **category** — `ad_search`
* **action** — `city_filter_geolocation_failed`

## City Filter Name Input Failed

* **category** — `ad_search`
* **action** — `city_filter_name_input_failed`
* **label** — `<city_name_query>`

## City Filter Reset

* **category** — `ad_search`
* **action** — `city_filter_reset`

## Transmission Filter Click

* **category** — `ad_search`
* **action** — `transmission_filter`

## Transmission Filter Selected

* **category** — `ad_search`
* **action** — `transmission_filter_selected`
* **label** — `<transmission>`

## Transmission Filter Reset

* **category** — `ad_search`
* **action** — `transmission_filter_reset`

## Fuel Filter Click

* **category** — `ad_search`
* **action** — `fuel_filter`

## Fuel Filter Selected

* **category** — `ad_search`
* **action** — `fuel_filter_selected`
* **label** — `<fuel>`

## Fuel Filter Reset

* **category** — `ad_search`
* **action** — `fuel_filter_reset`

## Price Filter Click

* **category** — `ad_search`
* **action** — `price_filter`

## Price Filter Selected

* **category** — `ad_search`
* **action** — `price_filter_selected`
* **label** — `<price>`

## Price Filter Reset

* **category** — `ad_search`
* **action** — `price_filter_reset`

## Year Filter Click

* **category** — `ad_search`
* **action** — `year_filter`

## Year Filter Selected

* **category** — `ad_search`
* **action** — `year_filter_selected`
* **label** — `<price>`

## Year Filter Reset

* **category** — `ad_search`
* **action** — `year_filter_reset`

## All Filters Reset

* **category** — `ad_search`
* **action** — `all_filters_reset`
