(ns drom-bot.worker
  (:require [clojure.core.async :refer [chan go-loop <!]]))

(defn spawn
  "Spawns a given number of async workers that receive jobs through a channel."
  [workers]
  (let [channel (chan)]
    (dotimes [n workers]
      (go-loop []
        (let [job (<! channel)]
          (job)
          (recur))))
    channel))
