(ns drom-bot.state
  (:require [taoensso.carmine :as car :refer [wcar]]))

(defmacro wcar* [& body] `(car/wcar {:pool {} :spec {:host (System/getenv "DROM_BOT_REDIS_HOST")
                                                     :port (Integer. (System/getenv "DROM_BOT_REDIS_PORT"))}}
                                    ~@body))

(defn sset [id state]
  (wcar* (car/set id state)))

(defn sget [id]
  (wcar* (car/get id)))
