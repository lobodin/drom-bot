(ns drom-bot.telegram-api
  (:require [clj-http.client :as http]
            [clojure.tools.logging :as log]
            [clojure.core.async :refer [chan go >!]]
            [clojure.data.json :as json]))

(def ^:private api-key (System/getenv "DROM_BOT_TELEGRAM_API_KEY"))
(def ^:private api-url "https://api.telegram.org/bot")
(def ^:private bot-url (str api-url api-key))

(def ^:private updates-url (str bot-url "/getUpdates"))
(def ^:private send-message-url (str bot-url "/sendMessage"))
(def ^:private send-photo-url (str bot-url "/sendPhoto"))
(def ^:private send-chat-action-url (str bot-url "/sendChatAction"))

(defn- fetch-updates
  "Syncroniously fetches updates with given offset and returns HTTP response."
  [offset]
  (let [url (format "%s?timeout=30&offset=%d" updates-url offset)]
    (log/info "fetch-update" url)
    (try
      (http/get url)
      (catch Exception e (log/error "fetch-updates" (.getMessage e))))))

(defn- parse-updates
  "Returns seq of updates or nil if none."
  [response]
  (when-let [body (:body response)]
    (when-let [updates (get (json/read-str body) "result")]
      (when (seq updates)
        updates))))

(defn- next-offset
  "Next offset is the biggest update id incremented by one."
  [updates]
  (inc (get (last updates) "update_id")))

(defn get-updates-channel
  "Returns channel that would be used for putting received updates."
  []
  (let [channel (chan)]
    (go (loop [offset 0]
          (if-let [updates (parse-updates (fetch-updates offset))]
            (do (>! channel updates)
                (recur (next-offset updates)))
            (recur offset))))
    channel))

(defn send-action [chat-id action]
  (let [body (json/write-str {"chat_id" chat-id
                              "action" action})]
    (http/post send-chat-action-url {:body body
                                     :content-type :json})))

(defn send-text [chat-id text reply-markup]
  (log/info "send-text" chat-id text)
  (send-action chat-id "typing")
  (let [body (json/write-str {"chat_id" chat-id
                              "text" text
                              "disable_web_page_preview" true
                              "reply_markup" (merge reply-markup
                                                    {"resize_keyboard" true})})]
    (http/post send-message-url {:body body
                                 :content-type :json})))

(defn send-photo [chat-id photo]
  (log/info "send-photo" chat-id)
  (let [response (http/post send-photo-url {:multipart [{:part-name "chat_id" :content (str chat-id)}
                                                        {:part-name "photo" :name "car.jpg" :content photo}]})
        data (json/read-str (:body response))]
    (get (first (get (get data "result") "photo")) "file_id")))
