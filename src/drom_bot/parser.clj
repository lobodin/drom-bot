(ns drom-bot.parser)

(defn- parse-range [raw]
  (loop [tokens (clojure.string/split (clojure.string/lower-case (clojure.string/trim raw)) #"[^\p{L}\d\.]")
         from nil
         to nil]
    (cond (empty? tokens) {:from from :to to}
          (empty? (first tokens)) (recur (rest tokens) from to)
          (or (= (first tokens) "от")
              (= (first tokens) "с")) (let [afrom (clojure.edn/read-string (second tokens))]
                                        (if (number? afrom)
                                          (recur (rest (rest tokens)) afrom to)
                                          (recur (rest tokens) from to)))
          (or (= (first tokens) "до")
              (= (first tokens) "по")) (let [ato (clojure.edn/read-string (second tokens))]
                                         (if (number? ato)
                                           (recur (rest (rest tokens)) from ato)
                                           (recur (rest tokens) from to)))
          :else (let [token (try (clojure.edn/read-string (first tokens))
                                 (catch Exception e nil))]
                  (if (number? token)
                    (if (nil? from)
                      (recur (rest tokens) token token)
                      (recur (rest tokens) from token))
                    (recur (rest tokens) from to))))))

(defn parse-price [raw]
  (parse-range raw))

(defn parse-year [raw]
  (parse-range raw))
