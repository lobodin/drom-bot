(ns drom-bot.core
  (:gen-class)
  (:require [clojure.core.async :refer [go <!! >!! chan]]
            [clojure.tools.logging :as log]
            [drom-bot.handler :as handler]
            [drom-bot.worker :as worker]
            [drom-bot.telegram-api :as telegram]
            [drom-bot.ner :as ner]))

(def ^:private workers 50)

(defn- process-updates [updates workers-channel]
  (doseq [current updates]
    (log/info "update" current)
    (let [chat-id (get-in current ["message" "chat" "id"])
          text (get-in current ["message" "text"])
          lat (get-in current ["message" "location" "latitude"])
          lon (get-in current ["message" "location" "longitude"])
          message (merge (when text {:text text})
                         (when (and lat lon) {:location {:lat lat :lon lon}}))
          job (handler/handle chat-id message)]
      (when job
        (>!! workers-channel job)))))

(defn -main [& args]
  (log/info "Building search index...")
  (ner/build-index)
  (let [updates-channel (telegram/get-updates-channel)
        workers-channel (worker/spawn workers)]
    (log/info "Listening for telegram updates.")
    (while true
      (let [updates (<!! updates-channel)]
        (process-updates updates workers-channel)))))
