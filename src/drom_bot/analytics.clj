(ns drom-bot.analytics
  (:require [clojure.core.async :refer [go >!]]
            [clj-http.client :as http]
            [drom-bot.worker :as worker]))

(def ^:private workers 5)
(def ^:private ga-version 1)
(def ^:private ga-tracking-id (System/getenv "DROM_BOT_ANALYTICS"))
(def ^:private ga-url "https://www.google-analytics.com/collect")

(def workers-channel (worker/spawn workers))

(defn track [cid category action label]
  (go (>! workers-channel
          #(http/post ga-url {:form-params {"v" ga-version
                                            "tid" ga-tracking-id
                                            "cid" cid
                                            "t" "event"
                                            "ec" category
                                            "ea" action
                                            "el" label}}))))
