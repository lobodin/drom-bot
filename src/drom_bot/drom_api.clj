(ns drom-bot.drom-api
  (:require [clj-http.client :as http]
            [clj-time.coerce :refer [from-long]]
            [clj-time.format :refer [formatter unparse]]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [pandect.algo.sha256 :refer [sha256]]))

(def ^:private drom-search-url "https://api.drom.ru/v1.1/bulls/search?")
(def ^:private drom-geocoding-url "https://api.drom.ru/v1.1/geo/whereami?")
(def ^:private app-id (System/getenv "DROM_BOT_DROM_APP_ID"))
(def ^:private app-token (System/getenv "DROM_BOT_DROM_TOKEN"))

(defn- encrypt [values]
  (sha256 (str (string/join values) app-token)))

(defn- build-url [base qs]
  (let [sorted-qs (into (sorted-map-by #(compare %1 %2)) qs)
        secret (encrypt (vals sorted-qs))
        encrypted-qs (assoc qs "secret" secret)]
    (str base
         (string/join "&" (map #(format "%s=%s" (first %) (second %))
                               encrypted-qs)))))

(defn- fetch-ads [brand model filters]
  (let [system-qs {"app_id" app-id
                   "timestamp" (quot (System/currentTimeMillis) 1000)
                   "order" "enterdate"
                   "order_d" "desc"
                   "photo_prefix" "ttn_320"}
        brand-qs {"fid" brand}
        model-qs (when model {"mid" model})
        filters-qs (apply hash-map
                          (flatten (remove nil?
                                           (map (fn [f]
                                                  (cond (= (first f) :transmission) ["transmission"
                                                                                     (get {"manual" 1 "automatic" 2} (second f))]
                                                        (= (first f) :gas) ["fueltype"
                                                                            (get {"patrol" 1 "diesel" 2} (second f))]
                                                        (= (first f) :city) ["cid"
                                                                             (:city-id (second f))]
                                                        (= (first f) :price) (let [price (second f)
                                                                                   from (:from price)
                                                                                   to (:to price)]
                                                                               (concat (when from ["minprice" from])
                                                                                       (when to ["maxprice" to])))
                                                        (= (first f) :year) (let [year (second f)
                                                                                  from (:from year)
                                                                                  to (:to year)]
                                                                              (concat (when from ["minyear" from])
                                                                                      (when to ["maxyear" to])))))
                                                filters))))
        qs (merge system-qs brand-qs model-qs filters-qs)
        url (build-url drom-search-url qs)]
    (try
      (http/get url)
      (catch Exception e (log/error "fetch-ads" url (.getMessage e))))))

(defn geocode [lat lon]
  (let [url (build-url drom-geocoding-url {"app_id" app-id
                                           "timestamp" (quot (System/currentTimeMillis) 1000)
                                           "lat" lat
                                           "lon" lon})
        response (try
                   (http/get url)
                   (catch Exception e (log/error "geocode" url (.getMessage e))))]
    (when-let [body (:body response)]
      (get (try
             (json/read-str body)
             (catch Exception e (do (log/error "geocode" (.getMessage e))
                                    {})))
                "idCity"))))

(defn get-ads [brand model filters]
  (when-let [response (:body (fetch-ads brand model filters))]
    (get-in (try
              (json/read-str response)
              (catch Exception e (do (log/error "get-ads" (.getMessage e))
                                     {})))
              ["data" "cars"])))

(defn- format-brand-model [ad]
  (format "%s %s" (get ad "firm_name") (get ad "model_name")))

(defn- format-date [ad]
  (let [date (get ad "enterdate")]
    (unparse (formatter "dd.MM.yyyy") (from-long (* date 1000)))))

(defn- format-year [ad]
  (get ad "year"))

(defn- format-city [ad]
  (get ad "city_name"))

(defn- format-price [ad]
  (string/replace (format "%,d руб." (get ad "price")) "," " "))

(defn- format-volume [ad]
  (let [volume (get ad "volume")]
    (when (> volume 0)
      (if (= (mod volume 1000) 0)
        (format "%s л" (/ volume 1000))
        (format "%s см3" volume)))))

(defn- format-fuel-type [ad]
  (let [fuel-type (get ad "fueltype")]
    (cond (= fuel-type 1) "бензин"
          (= fuel-type 2) "дизель")))

(defn- format-transmission [ad]
  (let [transmission (get ad "transmission")]
    (cond (= transmission 1) "механика"
          (= transmission 2) "автомат")))

(defn- format-drive [ad]
  (let [drive (get ad "privod")]
    (cond (= drive 1) "передний"
          (= drive 2) "задний"
          (= drive 3) "4WD")))

(defn format-ad [ad]
  (str (string/join ", " (filter #(not (nil? %)) [(format-brand-model ad)
                                                  (format-year ad)
                                                  (format-city ad)
                                                  (format-date ad)
                                                  (format-price ad)
                                                  (format-volume ad)
                                                  (format-fuel-type ad)
                                                  (format-transmission ad)
                                                  (format-drive ad)]))
       (format ".\nСмотреть объявление: %s.drom.ru" (get ad "id"))))
