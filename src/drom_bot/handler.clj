(ns drom-bot.handler
  (:require [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [drom-bot.state :as state]
            [drom-bot.telegram-api :as telegram]
            [drom-bot.drom-api :as drom]
            [drom-bot.analytics :as analytics]
            [drom-bot.ner :as ner]
            [drom-bot.parser :as parser]))

;;; Declarations.

(declare handle-ad-search)
(declare format-price-filter)
(declare format-year-filter)

;;; String table.

(def ^:private str-greeting "Привет! Я бот Drom.ru - напиши марку и модель и получи объявления о продаже авто. Например, Toyota Corolla.")
(def ^:private str-nothing-found "К сожалению, я ничего не нашёл 😢 Нужно писать английскими буквами и без опечаток 🕵")
(def ^:private str-nothing-found-on-drom "Не нашёл объявлений с такими параметрами на Drom.ru. Попробуй ввести другое название авто или сбросить фильтры.")
(def ^:private str-no-more-ads "Закончились объявления по вашему запросу.")
(def ^:private str-cta "Напиши марку и модель и я покажу тебе объявления о продаже авто!")
(def ^:private str-stop "⏹ Стоп")
(def ^:private str-next "➡️ Далее")
(def ^:private str-gas-patrol "Бензин")
(def ^:private str-gas-diesel "Дизель")
(def ^:private str-filters-info "Включены фильтры: %s")
(def ^:private str-filters-reset-action "Сбросить все фильтры")
(def ^:private str-filters-reset-result "Все фильтры сброшены.")
(def ^:private str-transmission-mechanical "Механика")
(def ^:private str-transmission-automatical "Автомат")
(def ^:private str-price-filter-menu "Фильтр по цене")
(def ^:private str-price-filter-prompt "Укажи диапозон цен. Например, от 300000 до 500000.")
(def ^:private str-price-filter-result "Показываю авто %s")
(def ^:private str-price-filter-reset-action "Сбросить фильтр по цене")
(def ^:private str-price-filter-reset-result "Фильтр по цене сброшен.")
(def ^:private str-price-filter-fail "Я не смог понять такую цену 😢")
(def ^:private str-year-filter-menu "Фильтр по году")
(def ^:private str-year-filter-prompt "Укажи годы авто. Например, 2014-2016.")
(def ^:private str-year-filter-result "Показываю авто %s.")
(def ^:private str-year-filter-reset-action "Сбросить фильтр по году")
(def ^:private str-year-filter-reset-result "Фильтр по году сброшен.")
(def ^:private str-year-filter-fail "Я не смог понять такие года😢")
(def ^:private str-transmission-filter-menu "Фильтр по КПП")
(def ^:private str-transmission-filter-prompt "Какую коробку передач ищем?")
(def ^:private str-transmission-filter-result "Показываю авто с %s КПП.")
(def ^:private str-transmission-filter-reset-result "Фильтр по КПП сброшен.")
(def ^:private str-transmission-filter-reset-action "Сбросить фильтр по КПП")
(def ^:private str-city-filter-menu "Фильтр по городу")
(def ^:private str-city-filter-prompt "Введи название города или отправь мне геолокацию.")
(def ^:private str-city-filter-result "Включён фильтр по городу.")
(def ^:private str-city-filter-reset-result "Фильтр по городу сброшен.")
(def ^:private str-city-filter-reset-action "Сбросить фильтр по городу")
(def ^:private str-city-filter-fail "Я не смог найти твой город 😢 ")
(def ^:private str-city-filter-send "Отправить геолокацию")
(def ^:private str-gas-filter-menu "Фильтр по типу топлива")
(def ^:private str-gas-filter-prompt "Какой тип топлива должен быть у авто?")
(def ^:private str-gas-filter-result "Показываю авто с %s.")
(def ^:private str-gas-filter-reset-result "Фильтр по типу топлива сброшен.")
(def ^:private str-gas-filter-reset-action "Сбросить фильтр по типу топлива")
(def ^:private str-feedback-prompt "Напиши мне что-нибудь, и я обязательно это прочитаю!")
(def ^:private str-feedback-result "Спасибо 😊")
(def ^:private str-help-prompt "Выбери вопрос из списка.")
(def ^:private str-help-auto-button "Как искать авто?")
(def ^:private str-help-auto "Чтобы найти авто, вводите английскими буквами марку или модель авто. Например \"toyota corolla\". Пока я не понимаю запросы русскими буквами и сленг автолюбителей, но активно учусь.")

;;; Send functions.

(defn- send-photo-message [chat-id photo]
  (telegram/send-action chat-id "upload_photo")
  (telegram/send-photo chat-id @photo))

(defn- send-text-message [chat-id text]
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "text"})))
  (telegram/send-text chat-id
                      text
                      {"hide_keyboard" true}))

(defn- send-ad [chat-id text ads index filters]
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "ad"
                                     :query text
                                     :index index
                                     :filters filters
                                     :ads ads})))
  (let [has-more? (< (inc index) (count ads))
        has-this? (< index (count ads))]
    (when has-this?
      (let [ad (nth ads index)]
        (when-let [photo (get ad "photo")]
          (send-photo-message chat-id
                              (delay (clojure.java.io/input-stream photo))))
        (telegram/send-text chat-id
                            (drom/format-ad ad)
                            {"keyboard" (concat [[{"text" str-stop} {"text" str-next}]
                                                 [{"text" str-city-filter-menu}]
                                                 [{"text" str-price-filter-menu}]
                                                 [{"text" str-year-filter-menu}]
                                                 [{"text" str-transmission-filter-menu}]
                                                 [{"text" str-gas-filter-menu}]]
                                                (when-not (empty? filters)
                                                  [[{"text" str-filters-reset-action}]]))})))
    (when-not has-more?
      (telegram/send-text chat-id str-no-more-ads {}))))

;;; Filters functions.

(defn- handle-filter-selected [chat-id text filter-result]
  (telegram/send-text chat-id filter-result {"hide_keyboard" true})
  (handle-ad-search chat-id text false))

(defn- handle-all-filters-reset [chat-id text]
  (let [chat (or (state/sget chat-id) {})]
    (analytics/track chat-id "ad_search" "all_filters_reset" nil)
    (state/sset chat-id (assoc chat :filters {}))
    (send-text-message chat-id str-filters-reset-result)
    (when (= (:last chat) "ad")
      (handle-ad-search chat-id (:query chat) false))))

;;; Ad search. 

(defn- handle-ad-search-next [chat-id text]
  (let [chat (state/sget chat-id)
        index (inc (or (:index chat) 0))
        ok (when (and chat
                      (= (:last chat) "ad"))
             (analytics/track chat-id "ad_search" "next" (:query chat))
             (send-ad chat-id (:query chat) (:ads chat) index (:filters chat))
             true)]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-ad-search-stop [chat-id text]
  (analytics/track chat-id "ad_search" "stop" nil)
  (send-text-message chat-id str-cta))

(defn- handle-ad-search [chat-id text show-filters?]
  (let [chat (state/sget chat-id)
        filters (:filters chat)
        entity (ner/recognize-car text)]
    (analytics/track chat-id "ad_search" "search" text)
    (if entity
      (let [ads (seq (drom/get-ads (:brand-id entity)
                                   (:model-id entity)
                                   filters))]
        (state/sset chat-id (merge chat {:last "ad"
                                         :query text
                                         :index 0
                                         :ads []}))
        (if ads
          (do (when (and show-filters? (seq filters))
                (let [filters-string-list (remove nil?
                                                  (map #(cond (= (first %) :transmission) (if (= (second %) "manual") "механика" "автомат")
                                                              (= (first %) :city) (:city-text (second %))
                                                              (= (first %) :gas) (if (= (second %) "patrol") "бензин" "дизель")
                                                              (= (first %) :price) (format-price-filter (second %))
                                                              (= (first %) :year) (format-year-filter (second %)))
                                                       filters))
                      filters-string (format str-filters-info 
                                             (clojure.string/join ", " filters-string-list))]
                  (telegram/send-text chat-id filters-string {})))
              (send-ad chat-id text ads 0 filters))
          (do (analytics/track chat-id "ad_search" "not_found" text)
              (telegram/send-text chat-id
                                  str-nothing-found-on-drom
                                  {"keyboard" [[{"text" str-filters-reset-action}]]}))))
      (do (analytics/track chat-id "ad_search" "recognition_failed" text)
          (send-text-message chat-id str-nothing-found)))))

;;; Transmission filter.

(defn- handle-transmission-filter-selected [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "ad")
               (let [filters (:filters chat)
                     transmission (cond (= text str-transmission-mechanical) "manual"
                                 (= text str-transmission-automatical) "automatic")
                     string (cond (= text str-transmission-mechanical) "механической"
                                  (= text str-transmission-automatical) "автоматической")]
                 (analytics/track chat-id "ad_search" "fuel_filter_selected" nil)
                 (state/sset chat-id (assoc chat :filters (merge filters
                                                                 {:transmission transmission})))
                 (handle-filter-selected chat-id
                                         (:query chat)
                                         (format str-transmission-filter-result string))
                 true)))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-transmission-filter-reset [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "ad")
               (analytics/track chat-id "ad_search" "transmission_filter_reset" nil)
               (state/sset chat-id (assoc chat :filters (dissoc (:filters chat)
                                                                :transmission)))
               (handle-filter-selected chat-id
                                       (:query chat)
                                       str-transmission-filter-reset-result)
               true))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-transmission-filter [chat-id text]
  (analytics/track chat-id "ad_search" "transmission_filter" nil)
  (telegram/send-text chat-id
                      str-transmission-filter-prompt
                      {"keyboard" [[{"text" str-transmission-mechanical}]
                                   [{"text" str-transmission-automatical}]
                                   [{"text" str-transmission-filter-reset-action}]]}))

;;; Price filter.

(defn- format-price-filter [price]
  (let [from (:from price)
        to (:to price)]
    (cond (and (some? from) (some? to)) (format "от %s до %s руб." from to)
          (some? from) (format "от %s руб." from)
          (some? to) (format "до %s руб." to))))

(defn- handle-price-filter-selected [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "price")
               (let [price (parser/parse-price text)
                     filters (:filters chat)
                     from (:from price)
                     to (:to price)
                     string (format-price-filter price)]
                 (if (nil? string)
                   (telegram/send-text chat-id str-price-filter-fail {})
                   (do
                     (state/sset chat-id (assoc chat :filters (merge filters {:price price})))
                     (handle-filter-selected chat-id
                                             (:query chat)
                                             (format str-price-filter-result string))
                     (analytics/track chat-id "ad_search" "price_filter_selected" text)
                     true)))))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-price-filter-reset [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "price")
               (analytics/track chat-id "ad_search" "price_filter_reset" nil)
               (state/sset chat-id (assoc chat :filters (dissoc (:filters chat)
                                                                :price)))
               (handle-filter-selected chat-id
                                       (:query chat)
                                       str-price-filter-reset-result)
               true))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-price-filter [chat-id text]
  (analytics/track chat-id "ad_search" "price_filter" nil)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "price"})))
  (telegram/send-text chat-id
                      str-price-filter-prompt
                      {"keyboard" [[{"text" str-price-filter-reset-action}]]}))

;;; Year filter.

(defn- format-year-filter [year]
  (let [from (:from year)
        to (:to year)]
    (cond (and (some? from) (some? to) (= from to)) (format "%s года" from)
          (and (some? from) (some? to)) (format "с %s по %s год включительно" from to)
          (some? from) (format "c %s года" from)
          (some? to) (format "по %s год включительно" to))))

(defn- handle-year-filter-selected [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "year")
               (let [year (parser/parse-year text)
                     filters (:filters chat)
                     from (:from year)
                     to (:to year)
                     string (format-year-filter year)]
                 (if (nil? string)
                   (telegram/send-text chat-id str-year-filter-fail {})
                   (do
                     (state/sset chat-id (assoc chat :filters (merge filters {:year year})))
                     (handle-filter-selected chat-id
                                             (:query chat)
                                             (format str-year-filter-result string))
                     (analytics/track chat-id "ad_search" "year_filter_selected" text)
                     true)))))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-year-filter-reset [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "year")
               (analytics/track chat-id "ad_search" "year_filter_reset" nil)
               (state/sset chat-id (assoc chat :filters (dissoc (:filters chat)
                                                                :year)))
               (handle-filter-selected chat-id
                                       (:query chat)
                                       str-year-filter-reset-result)
               true))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-year-filter [chat-id text]
  (analytics/track chat-id "ad_search" "year_filter" nil)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "year"})))
  (telegram/send-text chat-id
                      str-year-filter-prompt
                      {"keyboard" [[{"text" str-year-filter-reset-action}]]}))

;;; City filter.

(defn- handle-city-filter-selected [chat-id city source]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "city")
               (if city
                 (do (state/sset chat-id (assoc chat :filters (merge (:filters chat)
                                                                     {:city city})))
                     (handle-filter-selected chat-id
                                             (:query chat)
                                             str-city-filter-result))
                 (do (handle-filter-selected chat-id
                                             (:query chat)
                                             str-city-filter-fail)
                     (analytics/track chat-id
                                      "ad_search"
                                      (get {:search "city_filter_name_input_failed"
                                            :location "city_filter_geolocation_failed"}
                                           source)
                                      nil)))
               true))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-city-filter-reset [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "city")
               (analytics/track chat-id "ad_search" "city_filter_reset" nil)
               (state/sset chat-id (assoc chat :filters (dissoc (:filters chat)
                                                                :city)))
               (handle-filter-selected chat-id
                                       (:query chat)
                                       str-city-filter-reset-result)
               true))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-city-search [chat-id text]
  (analytics/track chat-id "ad_search" "city_filter_name_input" nil)
  (handle-city-filter-selected chat-id
                               (ner/recognize-city text)
                               :search))

(defn- handle-city-filter [chat-id text]
  (analytics/track chat-id "ad_search" "city_filter" nil)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "city"})))
  (telegram/send-text chat-id
                      str-city-filter-prompt
                      {"keyboard" [[{"text" str-city-filter-send
                                     "request_location" true}]
                                   [{"text" str-city-filter-reset-action}]]}))

;;; Gas filter.

(defn- handle-gas-filter-selected [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "ad")
               (let [filters (:filters chat)
                     gas (cond (= text str-gas-patrol) "patrol"
                               (= text str-gas-diesel) "diesel")
                     string (cond (= text str-gas-patrol) "бензином"
                                  (= text str-gas-diesel) "дизелем")]
                 (state/sset chat-id (assoc chat :filters (merge filters {:gas gas})))
                 (handle-filter-selected chat-id
                                         (:query chat)
                                         (format str-gas-filter-result string))
                 (analytics/track chat-id "ad_search" "fuel_filter_selected" nil)
                 true)))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

(defn- handle-gas-filter [chat-id text]
  (analytics/track chat-id "ad_search" "fuel_filter" nil)
  (telegram/send-text chat-id
                      str-gas-filter-prompt
                      {"keyboard" [[{"text" str-gas-patrol}]
                                   [{"text" str-gas-diesel}]
                                   [{"text" str-gas-filter-reset-action}]]}))

(defn- handle-gas-filter-reset [chat-id text]
  (let [ok (when-let [chat (state/sget chat-id)]
             (when (= (:last chat) "ad")
               (analytics/track chat-id "ad_search" "fuel_filter_reset" nil)
               (state/sset chat-id (assoc chat :filters (dissoc (:filters chat)
                                                                :gas)))
               (handle-filter-selected chat-id
                                       (:query chat)
                                       str-gas-filter-reset-result)
               true))]
    (when (not ok)
      (send-text-message chat-id str-nothing-found))))

;;; Bot introduction. 

(defn- handle-start [chat-id text]
  (analytics/track chat-id "command" "start" nil)
  (if-let [photo (state/sget "start-photo")]
    (telegram/send-photo chat-id photo)
    (let [photo (telegram/send-photo chat-id
                                     (-> "hello_mouse.png" io/resource clojure.java.io/input-stream))]
      (state/sset "start-photo" photo)))
  (send-text-message chat-id str-greeting))

;;; Feedback.

(defn- handle-feedback-submit [chat-id text]
  (analytics/track chat-id "command" "feedback_submit" text)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (dissoc chat :last)))
  (telegram/send-text chat-id str-feedback-result {}))

(defn- handle-feedback [chat-id text]
  (analytics/track chat-id "command" "feedback" nil)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "feedback"})))
  (telegram/send-text chat-id str-feedback-prompt {}))

;;; Help.

(defn- handle-help-submit [chat-id text]
  (analytics/track chat-id "command" "help_submit" text)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (dissoc chat :last)))
  (cond (= text str-help-auto-button) (send-text-message chat-id str-help-auto)
        :else (send-text-message chat-id str-cta)))

(defn- handle-help [chat-id text]
  (analytics/track chat-id "command" "help" nil)
  (let [chat (or (state/sget chat-id) {})]
    (state/sset chat-id (merge chat {:last "help"})))
  (telegram/send-text chat-id
                      str-help-prompt
                      {"keyboard" [[{"text" str-help-auto-button}]]}))

;;; Search.

(defn- handle-search [chat-id text]
  (analytics/track chat-id "command" "search" nil)
  (telegram/send-text chat-id str-cta {}))

;;; Higher level handlers.

(defn- handle-unmatched-text [chat-id text]
  (let [chat (state/sget chat-id)]
    (cond (= (:last chat) "city") (handle-city-search chat-id text)
          (= (:last chat) "price") (handle-price-filter-selected chat-id text)
          (= (:last chat) "year") (handle-year-filter-selected chat-id text)
          (= (:last chat) "feedback") (handle-feedback-submit chat-id text)
          (= (:last chat) "help") (handle-help-submit chat-id text)
          :else (handle-ad-search chat-id text true))))

(defn- handle-location [chat-id location]
  (analytics/track chat-id "ad_search" "city_filter_geolocation" nil)
  (handle-city-filter-selected chat-id
                               (drom/geocode (:lat location) (:lon location))
                               :location))

(defn- handle-text [chat-id text]
  (cond (= text "/start") #(handle-start chat-id text)
        (= text "/feedback") #(handle-feedback chat-id text)
        (= text "/help") #(handle-help chat-id text)
        (= text "/search") #(handle-search chat-id text)
        (= text str-next) #(handle-ad-search-next chat-id text)
        (= text str-stop) #(handle-ad-search-stop chat-id text)
        (= text str-transmission-filter-menu) #(handle-transmission-filter chat-id text)
        (= text str-city-filter-menu) #(handle-city-filter chat-id text)
        (= text str-city-filter-reset-action) #(handle-city-filter-reset chat-id text)
        (= text str-price-filter-menu) #(handle-price-filter chat-id text)
        (= text str-price-filter-reset-action) #(handle-price-filter-reset chat-id text)
        (= text str-year-filter-menu) #(handle-year-filter chat-id text)
        (= text str-year-filter-reset-action) #(handle-year-filter-reset chat-id text)
        (= text str-transmission-filter-reset-action) #(handle-transmission-filter-reset chat-id text)
        (= text str-gas-filter-menu) #(handle-gas-filter chat-id text)
        (= text str-filters-reset-action) #(handle-all-filters-reset chat-id text)
        (= text str-gas-filter-reset-action) #(handle-gas-filter-reset chat-id text)
        (or (= text str-transmission-mechanical)
            (= text str-transmission-automatical)) #(handle-transmission-filter-selected chat-id text)
        (or (= text str-gas-patrol)
            (= text str-gas-diesel)) #(handle-gas-filter-selected chat-id text)
        (not (nil? text)) #(handle-unmatched-text chat-id text)))

;;; The only public method. 

(defn handle [chat-id message]
  (log/info "handle" chat-id message)
  (cond (contains? message :location) (handle-location chat-id (:location message))
        (contains? message :text) (handle-text chat-id (:text message))))
