(ns drom-bot.ner
  (:import (org.apache.lucene.queryparser.classic QueryParser))
  (:require [clojure.java.io :as io]
            [clucy.core :as clucy]
            [clojure.data.json :as json]))

(def ^:private thesaurus (-> "thesaurus.json" io/resource slurp json/read-str))
(def ^:private car-index (clucy/memory-index))
(def ^:private city-index (clucy/memory-index))

(defn- build-brand-index [brands]
  (doseq [brand brands]
    (clucy/add car-index
               (with-meta {:brand-text (get brand "sMark")
                           :brand-id (get brand "idMark")}
                          {:brand-id {:indexed false}}))))

(defn- build-brand-model-index [brands models]
  (let [brands-map (reduce #(assoc %1 (get %2 :brand-id) %2)
                           {}
                           (map #(assoc {}
                                        :brand-text (get % "sMark")
                                        :brand-id (get % "idMark"))
                                brands))]
  (doseq [model models]
    (clucy/add car-index
               (with-meta (merge {:model-text (get model "sModel")
                                  :model-id (get model "idModel")}
                                 (get brands-map (get model "idMark")))
                          {:model-id {:indexed false}
                           :brand-id {:indexed false}})))))

(defn- build-city-index [cities]
  (doseq [city cities]
    (clucy/add city-index
               (with-meta {:city-text (get city "sCity")
                           :city-id (get city "idCity")}
                          {:city-id {:indexed false}}))))


(defn build-index []
  (let [brands (get (get thesaurus "Marks") "Mark")
        models (get (get thesaurus "Models") "Model")]
    (build-brand-model-index brands models)
    (build-brand-index brands))
  (let [cities (get (get thesaurus "Cities") "City")]
    (build-city-index cities)))

(defn recognize-car [text]
  (first (clucy/search car-index (QueryParser/escape text) 1)))

(defn recognize-city [text]
  (first (clucy/search city-index (QueryParser/escape text) 1)))
