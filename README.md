# Drom Bot

Drom.ru bot in Telegram for searching car ads in Russia.

## Features

* User starts the bot and receives greeting text and a mouse picture.
* User inputs brand name and/or model name and receives an ad or an error.
* User can show next ad.
* User can filter by city/transmission/fuel/year/price.
* User can input the city name manually or send current location.
* User can select transmission from several options.
* User can select fuel from several options.
* User can input price in semi-arbitrary form.
* User can input year in semi-arbitrary form.
* User can cancel one of the filters.
* User can cancel all filters at once.
* User is prompted to cancel all filters if drom.ru doesn't find searched car.
* User can cancel the search.

## Technical Notes

### Files Overview

* `state.clj` — API to Redis.
* `analytics.clj` — API to GA.
* `drom_api.clj` — API to drom.ru.
* `telegram_api.clj` — API to Telegram.
* `worker.clj` — function to spawn a given number of async workers that receive jobs through a channel.
* `ner.clj` — "named entity recognition" (just in-memory Lucene full-text search) algorithm that maps raw text to drom objects ID.
* `handler.clj` — takes user's query as input and produces a job to appropriately handle it. 
* `parser.clj` — extracts meaningful objects from text.
* `core.clj` — glues everything together; listens for telegram updates and assigns workers to handle them.

### Production Deployment

1. Install docker.
2. Clone the repo.
3. `cp .env-sample .env`
4. Edit `.env` file and set correct variable values.
5. `./deploy -f`
6. Add a cron job that would do `./deploy` every 5 minutes.

### Development

1. Install `leiningen`.
2. Launch Redis.
3. Export environmental variables:
  * `DROM_BOT_TELEGRAM_API_KEY`
  * `DROM_BOT_DROM_APP_ID`
  * `DROM_BOT_DROM_TOKEN`
  * `DROM_BOT_REDIS_HOST`
  * `DROM_BOT_REDIS_PORT`
  * `DROM_BOT_ANALYTICS`
4. `lein run` — will take several seconds because it needs to build the search index.

### Testing

1. `lein test`

## License

Copyright © 2016 Pavel Lobodin

Released under the MIT license.
