(ns drom-bot.telegram-api-test
  (:require [clojure.test :refer :all]
            [clj-http.fake :as fake]
            [drom-bot.telegram-api :as telegram]
            [clojure.core.async :refer [<!!]] ))

(deftest telegram-api

  (testing "Telegram API"

    (testing "updates channel puts telegram updates there"
      (fake/with-fake-routes
        {#".+getUpdates.+" (fn [request]
                             {:status 200 :headers {} :body "{\"result\": [{\"update_id\": 1, \"message\": {\"text\": \"subaru\", \"chat\": {\"id\": 100}}}]}"})}
        (let [updates-channel (telegram/get-updates-channel)]
          (let [first-update (<!! updates-channel)]
            (is (= (count first-update) 1))
            (is (= (first first-update) {"update_id" 1 "message" {"text" "subaru" "chat" {"id" 100}}}))))))))
