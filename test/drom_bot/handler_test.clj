(ns drom-bot.handler-test
  (:require [clojure.test :refer :all]
            [clojure.core.async :refer [>!! close!]]
            [clojure.java.io :as io] 
            [drom-bot.ner :as ner]
            [drom-bot.telegram-api :as telegram]
            [drom-bot.drom-api :as drom]
            [drom-bot.state :as state]
            [drom-bot.analytics :as analytics]
            [drom-bot.handler :as handler]))

(def state (atom {}))

(deftest handler-test

  (testing "Handler"

    (with-redefs [state/sset (fn [a b] (swap! state #(assoc % a b)))
                  state/sget (fn [a] (get @state a))
                  telegram/send-text (fn [a b c])
                  telegram/send-photo (fn [a b])
                  telegram/send-action (fn [a b])
                  drom/format-ad (fn [a])
                  drom/get-ads (fn [a b c])
                  analytics/track (fn [a b c d])
                  ner/recognize-car (fn [a])]

      (testing "/start sends a greeting photo file"
        (let [photo (atom nil)]
          (with-redefs [telegram/send-photo (fn [a b] (reset! photo b) "666")]
            ((handler/handle "1" {:text "/start"}))
            (is (= (slurp @photo)
                   (slurp (-> "hello_mouse.png" io/resource clojure.java.io/input-stream)))))))

      (testing "/start sends a greeting photo ID when already cached"
        (let [photo (atom nil)]
          (with-redefs [telegram/send-photo (fn [a b] (reset! photo b))]
            ((handler/handle "1" {:text "/start"}))
            (is (= @photo "666")))))

      (testing "/start sends a greeting text"
        (let [text (atom nil)]
          (with-redefs [telegram/send-text (fn [a b c] (reset! text b))]
            ((handler/handle "1" {:text "/start"}))
            (is (> (count @text) 0)))))

      (testing "`Tesla` query sends ad photo and ad text"
        (let [photo (atom nil)
              ad (atom nil)]
          (with-redefs [ner/recognize-car (fn [a]
                                            (is (= a "Tesla"))
                                            {:brand-id 10
                                             :model-id 100})
                        drom/get-ads (fn [a b c]
                                       (is (= a 10))
                                       (is (= b 100))
                                       [{"id" 777 "photo" "one"}
                                        {"id" 555}
                                        {"id" "987"}])
                        clojure.java.io/input-stream (fn [a]
                                                       (is (= a "one"))
                                                       "input-stream")
                        telegram/send-photo (fn [a b]
                                             (is (= b "input-stream"))
                                             (reset! photo b))
                        drom/format-ad (fn [a]
                                         (is (= a {"id" 777 "photo" "one"}))
                                         "formatted ad")
                        telegram/send-text (fn [a b c]
                                             (is (= b "formatted ad"))
                                             (reset! ad b))]
            ((handler/handle "123" {:text "Tesla"}))
            (is (= @photo "input-stream")
                (= @ad "formatted ad")))))

      (testing "`➡️ Далее` sends next ad"
        (let [ad (atom nil)]
          (with-redefs [drom/format-ad (fn [a]
                                         (is (= a {"id" 555}))
                                         "yep")
                        telegram/send-text (fn [a b c]
                                             (is (= b "yep"))
                                             (reset! ad b))]
            ((handler/handle "123" {:text "➡️ Далее"}))
            (is (= @ad "yep")))))

      (testing "`⏹ Стоп` resets search cycle"
        (let [text (atom nil)]
          (with-redefs [telegram/send-text (fn [a b c]
                                             (reset! text b))]
            ((handler/handle "123" {:text "⏹ Стоп"}))
            (is (> (count @text) 0))))))))
