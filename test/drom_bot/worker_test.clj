(ns drom-bot.worker-test
  (:require [clojure.test :refer :all]
            [clojure.core.async :refer [>!! close!]]
            [drom-bot.worker :as worker]))

(deftest worker-test

  (testing "Workers"

    (testing "are actually spawned"
      (let [channel (worker/spawn 1)
            counter (atom 0)
            mutate #(swap! counter inc)]
        (>!! channel mutate)
        (>!! channel mutate)
        (>!! channel mutate)
        (Thread/sleep 10)
        (is (= @counter 3))))))
