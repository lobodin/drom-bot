(ns drom-bot.ner-test
  (:require [clojure.test :refer :all]
            [drom-bot.ner :as ner]))

(deftest ner-tet
  (ner/build-index)

  (testing "Named Entity Recognition"

    (testing "cars"

      (testing "only brand is recognized when text includes just brand"
        (is (= {:brand-id "10" :brand-text "tesla"}
               (ner/recognize-car "Tesla"))))

      (testing "brand and model are recognized when query includes both"
        (is (= {:brand-id "10" :brand-text "tesla" :model-id "100" :model-text "model S"}
               (ner/recognize-car "tesla MODEL"))))

      (testing "brand and model are recognized when query includes just model"
        (is (= {:brand-id "11" :brand-text "subaru" :model-id "102" :model-text "impreza"}
               (ner/recognize-car "impreza"))))

      (testing "passing weird characters does not result in an error"
        (is (nil? (ner/recognize-car ";';:!@#$%^&*(|/3`")))))))
