(ns drom-bot.parser-test
  (:require [clojure.test :refer :all]
            [drom-bot.parser :as parser]))

(deftest parser-test

  (testing "Price Parser"

    (testing "can parse от 1000 до 2000"
      (is (= {:from 1000 :to 2000}
             (parser/parse-price "от 1000 до 2000"))))

    (testing "can parse от 100000"
      (is (= {:from 100000 :to nil}
             (parser/parse-price "от 100000"))))

    (testing "can parse 100000"
      (is (= {:from 100000 :to 100000}
             (parser/parse-price "100000"))))

    (testing "can parse 10000 — 100000"
      (is (= {:from 10000 :to 100000}
             (parser/parse-price "10000 — 100000"))))

    (testing "can parse до 100"
      (is (= {:from nil :to 100}
             (parser/parse-price "до 100"))))

    (testing "can parse 100-200"
      (is (= {:from 100 :to 200}
             (parser/parse-price "100-200")))))

  (testing "Year Parser"

    (testing "can parse от 1992 до 2000"
      (is (= {:from 1992 :to 2000}
             (parser/parse-year "от 1992 до 2000"))))

    (testing "can parse с 1992 по 2000 года"
      (is (= {:from 1992 :to 2000}
             (parser/parse-year "с 1992 по 2000 года"))))

    (testing "can parse с 1992"
      (is (= {:from 1992 :to nil}
             (parser/parse-year "с 1992"))))

    (testing "can parse от 1992"
      (is (= {:from 1992 :to nil}
             (parser/parse-year "от 1992"))))

    (testing "can parse 2015"
      (is (= {:from 2015 :to 2015}
             (parser/parse-year "2015"))))

    (testing "can parse 2000 — 2010"
      (is (= {:from 2000 :to 2010}
             (parser/parse-year "2000 — 2010"))))

    (testing "can parse до 2000"
      (is (= {:from nil :to 2000}
             (parser/parse-year "до 2000"))))

    (testing "can parse по 2000"
      (is (= {:from nil :to 2000}
             (parser/parse-year "по 2000"))))

    (testing "can parse 2009-2019"
      (is (= {:from 2009 :to 2019}
             (parser/parse-year "2009-2019"))))))
