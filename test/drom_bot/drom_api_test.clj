(ns drom-bot.drom-api-test
  (:require [clojure.test :refer :all]
            [clj-http.fake :as fake]
            [drom-bot.drom-api :as drom]))

(deftest drom-api

  (testing "Drom API"

    (testing "sends request with correct query string keys"
      (fake/with-fake-routes
        {#".+api\.drom\.ru.+" (fn [request]
                                (let [qs (str (:query-string request))]
                                  (is (.contains qs "app_id="))
                                  (is (.contains qs "timestamp="))
                                  (is (.contains qs "fid=10"))
                                  (is (.contains qs "mid=20"))
                                  (is (.contains qs "order=enterdate"))
                                  (is (.contains qs "order_d=desc"))
                                  (is (.contains qs "secret="))
                                  (is (.contains qs "transmission=")))
                                {:status 200 :headers {} :body "{}"})}
        (drom/get-ads "10" "20" {:transmission "manual"})))

    (testing "returns an array of ads"
      (fake/with-fake-routes
        {#".+api\.drom\.ru.+" (fn [request]
                                {:status 200 :headers {} :body "{\"data\":{\"cars\":[{\"id\":22264476}]}}"})} 
        (let [ads (drom/get-ads "1" "2" nil)]
          (is (= ads [{"id" 22264476}])))))))
